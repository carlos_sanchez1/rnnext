import styles from './SideBar.module.css';
import Link from 'next/link';

const SideBar = () => {
  return (
    <div className={styles.container}>
      <div className={styles.containers}>
        <Link href="/study">
          <a>Study</a>
        </Link>
      </div>
      <div className={styles.containers}>
        <Link href="/tasks">
          <a>tasks</a>
        </Link>
      </div>
      <div className={styles.containers}>routines</div>
      <div className={styles.containers}>calendar</div>
      <div className={styles.containers}>settings</div>
    </div>
  );
};

export default SideBar;
