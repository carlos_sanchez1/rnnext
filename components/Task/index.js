import React, { useEffect, useState } from 'react';
import styles from './Task.module.css';

import axios from 'axios';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
// import {
//   faPlusCircle,
//   faRunning,
//   faMoon,
//   faUtensils,
// } from '@fortawesome/free-solid-svg-icons';

const Task = () => {
  const [userTasks, setUserTasks] = useState([]);

  useEffect(() => {
    async function getUser() {
      const url = `http://localhost:3500/mydomain/users/605e4daaab0af30c7ef0ed90/`;
      try {
        const userRes = await axios.get(url);
        setUserTasks(userRes.data.user.userTasks);
      } catch (error) {
        console.log(error);
      }
    }
    getUser();
  }, []);

  return (
    <div className={styles.container}>
      <div className={styles.tasks_info}>
        <div>today tasks</div>
        <div style={{ width: '10%' }}>
          {/* <FontAwesomeIcon color="red" icon={faPlusCircle} /> */}
        </div>
      </div>
      <div>
        {userTasks.map((item) => (
          <div
            style={{
              backgroundColor: item.color,
              display: 'flex',
              flexDirection: 'row',
            }}
          >
            <FontAwesomeIcon color="white" icon="ad" />
            <div>{item.name}</div>
          </div>
        ))}
      </div>
    </div>
  );
};

export default Task;
