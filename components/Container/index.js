import Navbar from '../Navbar';
import Head from 'next/head';
import SideBar from '../SideBar';
import styles from './Container.module.css';

const Container = (props) => {
  return (
    <>
      <Head>
        <title>conatiner title</title>
        <link
          rel="stylesheet"
          href="https://stackpath.bootstrapcdn.com/bootswatch/4.4.1/flatly/bootstrap.min.css"
        />
      </Head>
      <Navbar />
      <div className={styles.container}>
        <SideBar />
        <div className={styles.children_container}>{props.children}</div>
      </div>
    </>
  );
};

export default Container;
