import React, { useEffect, useState } from 'react';
import styles from './Calendar.module.css';

const Calendar = () => {
  const currentDate = new Date();
  const [modifiableMonth, setModifiableMonth] = useState(
    currentDate.getMonth()
  );
  const [modifiableYear, setModifiableYear] = useState(
    currentDate.getFullYear()
  );
  const [monthName, setMonthName] = useState('');
  const [modifiableDay, setModifiableDay] = useState(currentDate.getDate());

  const [days, setDays] = useState([]);
  const [dayTasks, setDayTasks] = useState([]);

  useEffect(() => {
    console.log('mes:', modifiableMonth);
    console.log(modifiableYear);
    console.log(modifiableDay);

    switch (modifiableMonth) {
      case 0:
        setMonthName('Enero');
        break;
      case 1:
        setMonthName('Feb');
        break;
      case 2:
        setMonthName('Marz');
        break;
      case 3:
        setMonthName('Abr');
        break;
      case 4:
        setMonthName('May');
        break;
      case 5:
        setMonthName('Juni');
        break;
      case 6:
        setMonthName('Jul');
        break;
      case 7:
        setMonthName('Ago');
        break;
      case 8:
        setMonthName('Sep');
        break;
      case 9:
        setMonthName('Oct');
        break;
      case 10:
        setMonthName('Nov');
        break;
      case 11:
        setMonthName('Dic');
        break;
      default:
        setMonthName('0000');
        break;
    }
    console.log(monthName);

    const handleLeapYear = () => {
      // eslint-disable-next-line prettier/prettier
      return (
        (modifiableYear % 100 !== 0 && modifiableYear % 4 === 0) ||
        modifiableYear % 400 === 0
      );
    };

    const handleMonthDays = () => {
      if (
        modifiableMonth === 0 ||
        modifiableMonth === 2 ||
        modifiableMonth === 4 ||
        modifiableMonth === 6 ||
        modifiableMonth === 7 ||
        modifiableMonth === 9 ||
        modifiableMonth === 11
      ) {
        return 31;
      } else if (
        modifiableMonth === 3 ||
        modifiableMonth === 5 ||
        modifiableMonth === 8 ||
        modifiableMonth === 10
      ) {
        return 30;
      } else {
        return handleLeapYear() ? 29 : 28;
      }
    };

    class dayobj {
      constructor(day, month, year, id, otherdaysmonth, dayWithTasks) {
        this.day = day;
        this.month = month;
        this.year = year;
        this.id = id;
        this.otherdaysmonth = otherdaysmonth;
        this.dayWithTasks = dayWithTasks;
      }
    }

    const handleWriteMonth = () => {
      const prevLastDay = new Date(
        modifiableYear,
        modifiableMonth,
        0
      ).getDate();

      const handleStartDayWeek = new Date(
        modifiableYear,
        modifiableMonth,
        1
      ).getDay();

      const lastDayIndex = new Date(
        modifiableYear,
        modifiableMonth + 1,
        0
      ).getDay();

      const nextDays = 7 - lastDayIndex - 1;

      const totaldays = [];
      for (let i = handleStartDayWeek; i > 0; i--) {
        totaldays.push(
          new dayobj(
            prevLastDay - i + 1,
            modifiableMonth - 1,
            modifiableYear,
            prevLastDay - i + 1,
            true
          )
        );
      }

      for (let i = 1; i <= handleMonthDays(); i++) {
        totaldays.push(
          new dayobj(
            i,
            modifiableMonth,
            modifiableYear,
            i,
            false,
            dayTasks.find(
              (day) =>
                day.soundDay === i &&
                day.soundMonth === currentMonth &&
                day.soundYear === currentYear
            ) === undefined
              ? false
              : true
          )
        );
      }

      for (let i = 1; i <= nextDays; i++) {
        totaldays.push(
          new dayobj(i, modifiableMonth + 1, modifiableYear, i, true)
        );
      }
      setDays(totaldays);
    };
    handleWriteMonth();
    console.log(days);
  }, [modifiableMonth]);

  const handleLastMonth = () => {
    if (modifiableMonth !== 0) {
      setModifiableMonth(modifiableMonth - 1);
    } else {
      setModifiableMonth(11);
      setModifiableCurrentYearNumber(modifiableCurrentYearNumber - 1);
    }
  };

  const handleNextMonth = () => {
    if (modifiableMonth !== 11) {
      setModifiableMonth(modifiableMonth + 1);
    } else {
      setModifiableMonth(0);
      setModifiableCurrentYearNumber(modifiableCurrentYearNumber + 1);
    }
  };

  return (
    <div className={styles.cont}>
      <div className={styles.calendar_info}>
        <div onClick={handleLastMonth} className={styles.prev_month}>
          &#9664;
        </div>
        <div className={styles.month}>{monthName}</div>
        <div className={styles.year}>{modifiableYear}</div>
        <div onClick={handleNextMonth} className={styles.next_month}>
          &#9654;
        </div>
      </div>

      <div className={styles.calendar_week}>
        <div className={styles.week_day}>D</div>
        <div className={styles.week_day}>L</div>
        <div className={styles.week_day}>M</div>
        <div className={styles.week_day}>Mi</div>
        <div className={styles.week_day}>J</div>
        <div className={styles.week_day}>V</div>
        <div className={styles.week_day}>S</div>
      </div>

      <div className={styles.calendar_month_days}>
        {days.map((item) =>
          item.day === currentDate.getDate() &&
          item.month === currentDate.getMonth() &&
          !item.otherdaysmonth ? (
            <div className={styles.current_day_month}>{item.day}</div>
          ) : (
            <div className={styles.month_days}>{item.day}</div>
          )
        )}
      </div>
    </div>
  );
};

export default Calendar;
