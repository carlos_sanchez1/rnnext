import React from 'react';
import Link from 'next/link';

import styles from './Navbar.module.css';

const Navbar = () => {
  return (
    <nav className={`${styles.nav} ${styles.bgimg}`}>
      <Link href="/">
        <a className={styles.brand}>Skool</a>
      </Link>
    </nav>
  );
};

export default Navbar;
