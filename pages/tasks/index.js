import React, { useEffect, useState } from 'react';

import Container from '../../components/Container';
import Calendar from '../../components/Calendar';
import Task from '../../components/Task';

import styles from '../../styles/Tasks.module.css';

const Tasks = () => {
  useEffect(() => {
    console.log('una vez bro');
  }, []);
  return (
    <Container>
      <div className={styles.container}>
        <div className={styles.calendar_container}>
          <Calendar />
        </div>
        <div className={styles.task_container}>
          <Task />
        </div>
      </div>
    </Container>
  );
};

export default Tasks;
