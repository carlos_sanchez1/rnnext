import React from 'react';
import Navbar from '../components/Navbar';

import Container from '../components/Container';

const About = () => {
  return (
    <Container>
      <h5>About</h5>
    </Container>
  );
};

export default About;
